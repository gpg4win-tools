/* Copyright (C) 2018 by Intevation GmbH
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */


#include <iostream>
#include "quitter.h"

#include <QApplication>

static void
rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

void Quitter::run()
{
    std::string line;
    while (true) {
        std::getline(std::cin, line);
        rtrim(line);
        if (std::cin.eof() || line == "quit") {
            qApp->quit();
            return;
        }
    }
}
