#ifndef OVERLAYER_OPTIONS
#define OVERLAYER_OPTIONS
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QCommandLineParser>
#include <QList>

/** @file Commandline options*/
static void options(QCommandLineParser &parser)
{
    QList<QCommandLineOption> options;

    options
        << QCommandLineOption(QStringList() << QStringLiteral("debug"),
                QStringLiteral("Print debug output."))
        << QCommandLineOption(QStringLiteral("hwnd"),
                QStringLiteral("Parent Window"),
                QStringLiteral("windows window handle"))
        << QCommandLineOption(QStringLiteral("overlayText"),
                QStringLiteral("Overlay Text"),
                QStringLiteral("text to overlay over hwnd"));

    for (const auto &opt: options) {
        parser.addOption(opt);
    }
    parser.addVersionOption();
    parser.addHelpOption();
}
#endif // OVERLAYER_OPTIONS
