#ifndef QUITTER_H
#define QUITTER_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QThread>
#include <iostream>

class Quitter: public QThread
{
    Q_OBJECT;

protected:
    virtual void run() override;
};

#endif // QUITTER_H
