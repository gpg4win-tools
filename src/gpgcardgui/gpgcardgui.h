/* Copyright (C) 2020 by g10 Code GmbH <info@g10code.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
#pragma once
#include <QApplication>
#include <QCommandLineParser>

class MainWindow;


/** @brief Resolver Application entry point.
 *
 * The GpgCardGUI exists to serve as a frontend
 * for gpgcard. It is written in a way that the
 * widgets are reusable in other applications. With
 * the goal to move most of the code to libkleo.
 *
 * This is the global qApp singleton. */
class GpgCardGUI: public QApplication
{
    Q_OBJECT

public:
    explicit GpgCardGUI(int &argc, char *argv[]);
    ~GpgCardGUI();

    void showWindow();

private:
    MainWindow *mMainWin;
};
