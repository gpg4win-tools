/* Copyright (C) 2020 by g10 Code GmbH <info@g10code.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
#pragma once
#include <QCommandLineParser>
#include <QList>

/** @file Commandline options*/
static void options(QCommandLineParser &parser)
{
    QList<QCommandLineOption> options;

    options
        << QCommandLineOption(QStringList() << QStringLiteral("debug"),
                QStringLiteral("Print debug output."))
        << QCommandLineOption(QStringLiteral("lang"),
                QStringLiteral("Language"),
                QStringLiteral("Language to be used e.g. de_DE"))
        ;

    for (const auto &opt: options) {
        parser.addOption(opt);
    }
    parser.addVersionOption();
    parser.addHelpOption();
}
