/* Copyright (C) 2020 by g10 Code GmbH <info@g10code.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
#include "gpgcardgui.h"
#include "mainwindow.h"

GpgCardGUI::GpgCardGUI(int &argc, char *argv[]):
    QApplication(argc, argv)
{
    mMainWin = new MainWindow();
}

GpgCardGUI::~GpgCardGUI()
{
    delete mMainWin;
}

void GpgCardGUI::showWindow()
{
    mMainWin->show();
}
