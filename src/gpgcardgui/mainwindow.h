/* Copyright (C) 2020 by g10 Code GmbH <info@g10code.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#pragma once
#include <QMainWindow>

#include <Libkleo/CardManager>

class QTextEdit;

/** MainWindow of the application. */
class MainWindow: public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr,
                        Qt::WindowFlags flags = Qt::WindowFlags());
private:
    QTextEdit *mEdit;
    Kleo::SmartCard::CardManager mManager;
};
