/* Copyright (C) 2020 by g10 Code GmbH <info@g10code.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include "mainwindow.h"

#include <QTextEdit>
#include <QString>
#include <QDebug>

#include <Libkleo/Card>
#include <Libkleo/CardManager>

#include "w32-gettext.h"

MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags):
    QMainWindow(parent, flags)
{
    mEdit = new QTextEdit (this);
    mEdit->setReadOnly(true);

    setCentralWidget(mEdit);

    connect (&mManager, &Kleo::SmartCard::CardManager::cardsMayHaveChanged, this, [this] () {
        const auto cards = mManager.cards();
        for (const auto card: cards) {
            if (card) {
                mEdit->setText(mEdit->toPlainText() +
                             "\nFound smartcard" + QString::fromStdString(card->serialNumber()) +
                             "\nIn Reader: " + card->reader());
            }
        }
    });

    mManager.startCardList();
}
