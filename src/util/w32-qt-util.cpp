/* Copyright (C) 2018 by Andre Heinecke <aheinecke@gnupg.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
#include <QWidget>
#include <QWindow>

#include "w32-qt-util.h"

void W32::setupForeignParent(WId id, QWidget *widget, bool modal)
{
    if (!widget || !id) {
        return;
    }

    auto foreignWindow = QWindow::fromWinId(id);
    widget->winId();
    auto parentHandle = widget->windowHandle();
    if (parentHandle && foreignWindow) {
        parentHandle->setTransientParent(foreignWindow);
        if (modal) {
            widget->setWindowModality(Qt::WindowModal);
        }
    }
}
