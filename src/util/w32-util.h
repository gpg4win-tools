/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <string>

/* The Registry key used by Gpg4win.  */
#ifdef _WIN64
# define GPG4WIN_REGKEY_2  "Software\\Wow6432Node\\GNU\\GnuPG"
#else
# define GPG4WIN_REGKEY_2  "Software\\GNU\\GnuPG"
#endif
#ifdef _WIN64
# define GPG4WIN_REGKEY_3  "Software\\Wow6432Node\\Gpg4win"
#else
# define GPG4WIN_REGKEY_3  "Software\\Gpg4win"
#endif

#define GPGOL_REG_PATH "Software\\GNU\\GpgOL"

namespace W32
{
/* Get the locale dir of Gpg4win. */

std::string getGpg4winLocaleDir();

/** Get the Gpg4win Install directory.
 *
 * Looks for the Gpg4win 3.x registry key.
 * And checks that the directory can be read.
 *
 * @returns an empty string if no dir could be found.
 *
 **/
std::string getGpg4winDir();

/** Read a registry string value. If root is null first
 * HKEY_CURRENT_USER is searched and then it falls back
 * to HKEY_LOCAL_MACHINE . */
std::string readRegStr(const char *root,
                       const char *path,
                       const char *key);

bool writeRegStr(const char *root,
                 const char *path,
                 const char *key,
                 const char *val);

/** Call this to switch to the W64 registry. */
void setW64RegistryMode(bool value);

bool isElevated();
} // namespace W32

/** Helper to convert a string 0 / 1 to a bool with a default.
 * If the value ends with ! the parameter forced is set to true*/
bool strToBool(const std::string &str, bool defaultVal, bool &forced);
