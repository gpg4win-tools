#ifndef OVERLAY_H
#define OVERLAY_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QWidget>
#include <memory>
class QPaintEvent;

class Overlay: public QWidget
{
    Q_OBJECT
public:
    Overlay (WId id, const QString &text);

protected:
    virtual void paintEvent (QPaintEvent *event) override;

private:
    class Private;
    std::shared_ptr<Private> d;
};
#endif
