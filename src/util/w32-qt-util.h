#ifndef W32_QT_UTIL_H
#define W32_QT_UTIL_H
/* Copyright (C) 2018 by Andre Heinecke <aheinecke@gnupg.com>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QWidget>

namespace W32
{
void setupForeignParent(WId id, QWidget *widget, bool modal);
} // namespace W32
#endif //W32-QT-UTIL_H
