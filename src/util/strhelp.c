/* Copyright (C) 2015 by ETH Zürich
 * Software engineering by Intevation GmbH
 *
 * This file is Free Software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
/* Needed to get asprintf */
#define _GNU_SOURCE 1

/** @file See strhelp.h for documentation. */

#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#ifdef WIN32
#include <windows.h>
#endif

static void
out_of_core(void)
{
  fputs("\nfatal: out of memory\n", stderr);
  exit(2);
}

void *
xmalloc( size_t n )
{
  void *p = malloc( n );
  if( !p )
    out_of_core();
  return p;
}

void *
xmalloc0( size_t n )
{
  void *p = malloc( n );
  if( !p )
    out_of_core();
  memset (p, 0, n);
  return p;
}

void *
xrealloc( void *a, size_t n )
{
  void *p = realloc( a, n );
  if( !p )
    out_of_core();
  return p;
}

char *
xstrndup( const char *string, const size_t len )
{
  char *p = xmalloc( len + 1 );
  memcpy( p, string, len );
  p[len] = '\0';
  return p;
}

unsigned int
strv_length (char **str_array)
{
  unsigned int i = 0;

  if (!str_array)
    return 0;

  while (str_array[i])
    ++i;

  return i;
}

void strv_append (char ***pArray, const char *string, const size_t len)
{
  unsigned int old_len = 0;

  if (!*pArray)
    {
      *pArray = xmalloc(2 * sizeof(char*));
      (*pArray)[0] = xstrndup(string, len);
      (*pArray)[1] = NULL;
      return;
    }
  old_len = strv_length(*pArray);
  *pArray = xrealloc(*pArray, sizeof(char**) * (old_len + 2));

  (*pArray)[old_len] = xstrndup(string, len);
  (*pArray)[old_len + 1] = NULL;
}

void
str_append_str(char **pDst, size_t *dst_len, const char *appendage, const size_t len)
{
  if (!appendage)
    return;

  if (!(*pDst))
    {
      *pDst = xstrndup(appendage, len);
      *dst_len = len;
    }
  else
    {
      size_t new_size = (*dst_len) + len + 1;
      char *p_old = *pDst;
      *pDst = xmalloc(new_size);
      strncpy(*pDst, p_old, *dst_len);
      strncpy(*pDst + *dst_len, appendage, len);
      *dst_len = new_size - 1;
      (*pDst)[*dst_len] = '\0';
      free (p_old);
    }
}

void
strv_free (char **str_array)
{
  if (str_array)
    {
      int i;

      for (i = 0; str_array[i] != NULL; i++)
        free (str_array[i]);

      free (str_array);
    }
}

bool
str_equal (char *s1, char *s2)
{
  size_t l1 = strlen(s1);
  size_t l2 = strlen(s2);
  if ((l1 == l2) &&
      (strcmp(s1, s2) == 0))
    return true;
  else
    return false;
}

bool
str_starts_with (char *s1, char *s2)
{
  size_t l2 = strlen(s2);
  if (strncmp(s1, s2, l2) == 0)
    return true;
  else
    return false;
}

void
str_trim (char **s)
{
  size_t i;
  if (*s != NULL)
    {
      while (isspace(**s))
        (*s)++;
      i = strlen(*s);
      while (isspace((*s)[--i]))
        (*s)[i] = '\0';
    }
}

void
xfree (void *p)
{
  if (p)
    free (p);
}

int
xasprintf (char **strp, const char *fmt, ...)
{
  int ret;
  va_list ap;
  va_start(ap, fmt);
  ret = vasprintf(strp, fmt, ap);
  va_end(ap);

  if (ret == -1)
    out_of_core();

  return ret;
}

#ifdef WIN32
/* Adapted from GPGOL rev. e512053 */
char *
wchar_to_utf8 (const wchar_t *string, size_t len)
{
  int n, ilen;
  char *result;

  ilen = (int) len;
  if (ilen < 0)
    return NULL;

  /* Note, that CP_UTF8 is not defined in Windows versions earlier
     than NT.*/
  n = WideCharToMultiByte (CP_UTF8, 0, string, ilen, NULL, 0, NULL, NULL);
  if (n < 0)
    return NULL;

  result = xmalloc ((size_t)n+1);
  n = WideCharToMultiByte (CP_UTF8, 0, string, ilen, result, n, NULL, NULL);
  if (n < 0)
    {
      xfree (result);
      return NULL;
    }
  result[n] = 0;
  return result;
}

/* Adapted from GPGOL rev. e512053 */
wchar_t *
utf8_to_wchar (const char *string, size_t len)
{
  int n, ilen;
  wchar_t *result;

  ilen = (int) len;
  if (ilen < 0)
    return NULL;

  n = MultiByteToWideChar (CP_UTF8, 0, string, ilen, NULL, 0);
  if (n < 0 || n + 1 < 0)
    return NULL;

  result = xmalloc ((size_t)(n+1) * sizeof *result);
  n = MultiByteToWideChar (CP_UTF8, 0, string, ilen, result, n);
  if (n < 0)
    {
      xfree (result);
      return NULL;
    }
  result[n] = 0;
  return result;
}

wchar_t
*acp_to_wchar (const char *string, size_t len)
{
  int n, ilen;
  wchar_t *result;

  ilen = (int) len;
  if (ilen < 0)
    return NULL;

  n = MultiByteToWideChar (CP_ACP, 0, string, ilen, NULL, 0);
  if (n < 0 || n + 1 < 0)
    return NULL;

  result = xmalloc ((size_t)(n+1) * sizeof *result);
  n = MultiByteToWideChar (CP_ACP, 0, string, ilen, result, n);
  if (n < 0)
    {
      xfree (result);
      return NULL;
    }
  result[n] = 0;
  return result;
}
#endif

