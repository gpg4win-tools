#ifndef W32_GETTEXT_H
#define W32_GETTEXT_H

/* w32-gettext.h - Adoption of a libgpg-errors w32-gettext
   Copyright (C) 2005 g10 Code GmbH
                 2018 Intevation GmbH <info@intevation.de>

   libgpg-error is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the free software Foundation; either version 2.1 of
   the License, or (at your option) any later version.

   libgpg-error is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with libgpg-error; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.  */

#ifdef __cplusplus
extern "C" {
#if 0
}
#endif
#endif

#ifdef _WIN32

const char *gettext (const char *msgid);
const char *utf8_gettext (const char *msgid);

/* Init i18n for the pkg and locate dir locale_dir.*/
void i18n_init (const char *pkg_name, const char *locale_dir);

#else // _WIN32

char *gettext (const char *msgid) noexcept;
const char *utf8_gettext (const char *msgid);
void i18n_init (const char *pkg_name, const char *locale_dir);
#endif //_WIN32

#define _(a) utf8_gettext (a)

#endif
#ifdef __cplusplus
}
#endif
