#ifndef GPGOLCONFIG_OPTIONS
#define GPGOLCONFIG_OPTIONS
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QCommandLineParser>
#include <QList>

/** @file Commandline options*/
static void options(QCommandLineParser &parser)
{
    QList<QCommandLineOption> options;

    options
        << QCommandLineOption(QStringList() << QStringLiteral("debug"),
                QStringLiteral("Print debug output."))
        << QCommandLineOption(QStringList() << QStringLiteral("w64"),
                QStringLiteral("GpgOL is a 64bit application."))
        << QCommandLineOption(QStringLiteral("hwnd"),
                QStringLiteral("Parent Window"),
                QStringLiteral("windows window handle"))
        << QCommandLineOption(QStringLiteral("lang"),
                QStringLiteral("Language"),
                QStringLiteral("Language to be used e.g. de_DE"))
        << QCommandLineOption(QStringLiteral("gpgol-version"),
                QStringLiteral("Version string"),
                QStringLiteral("GpgOL's Version"))
        << QCommandLineOption(QStringLiteral("alwaysShow"),
                              QStringLiteral("Should always be shown"));

    for (const auto &opt: options) {
        parser.addOption(opt);
    }
    parser.addVersionOption();
    parser.addHelpOption();
}
#endif
