/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include "gpgolconfig.h"
#include "w32-gettext.h"
#include "w32-util.h"
#include "w32-qt-util.h"
#include "gpgolconfigpage.h"
#include "gpgoldebugpage.h"
#include "cryptoconfigpage.h"

#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QCommandLineParser>
#include <QDebug>

#include <Libkleo/DocAction>

#include <KPageDialog>
#include <KGuiItem>
#include <KStandardGuiItem>

GpgOLConfig::GpgOLConfig(const QCommandLineParser &parser):
  KPageDialog(nullptr)
{
    setWindowFlags(windowFlags() & (~Qt::WindowContextHelpButtonHint));

    setWindowTitle(_("Configure GpgOL"));

    setWindowIcon(QIcon(":/gpgol-icon.svg"));

    const auto hwnd = parser.value(QStringLiteral("hwnd"));
    if (!hwnd.isEmpty()) {
        bool ok;
        WId id = (WId) hwnd.toInt(&ok);
        if (!ok) {
            qDebug() << "invalid hwnd value";
        } else {
            W32::setupForeignParent(id, this, true);
            setModal(true);
        }
    }

    if (parser.isSet("gpgol-version")) {
        mVersion = parser.value("gpgol-version");
    } else {
        mVersion = QStringLiteral("unknown version");
    }

    setupGUI();

    resize(800, 500);
}

void GpgOLConfig::setupGUI()
{
    setFaceType(KPageDialog::List);

    QDialogButtonBox *buttonBox = new QDialogButtonBox();
    buttonBox->setStandardButtons(QDialogButtonBox::RestoreDefaults |
                                  QDialogButtonBox::Cancel |
                                  QDialogButtonBox::Ok);
    KGuiItem::assign(buttonBox->button(QDialogButtonBox::Ok), KStandardGuiItem::ok());
    KGuiItem::assign(buttonBox->button(QDialogButtonBox::Cancel), KStandardGuiItem::cancel());
    KGuiItem::assign(buttonBox->button(QDialogButtonBox::RestoreDefaults),
                                       KStandardGuiItem::defaults());

    /* Inject VSD Help */
    const auto helpAction = new Kleo::DocAction(QIcon::fromTheme(QStringLiteral("help")),
            _("Help"),
    /* TRANSLATORS: Only available in german and english please keep english for other langs */
            _("handout_outlook_plugin_gnupg_en.pdf"),
            QStringLiteral("../share/doc/gnupg-vsd"));
    if (helpAction->isEnabled()) {
        auto helpButton = buttonBox->addButton(QDialogButtonBox::Help);
        if (helpButton) {
            disconnect(helpButton, &QAbstractButton::clicked, nullptr, nullptr);
            connect(helpButton, &QAbstractButton::clicked, helpAction, &QAction::trigger);
            connect(helpButton, &QObject::destroyed, helpAction, &QObject::deleteLater);
        }
    } else {
        delete helpAction;
    }

    setButtonBox(buttonBox);

    bool unused;
    bool hideCryptoConf = strToBool(W32::readRegStr(nullptr, GPGOL_REG_PATH, "hideCryptoConfig"),
                                    false, unused);

    auto cryptoConfWidget = hideCryptoConf ? nullptr : new CryptoConfigPage;
    auto gpgolConfWidget = new GpgOLConfigPage;
    auto gpgolDbgWidget = new GpgOLDebugPage;

    connect(buttonBox->button(QDialogButtonBox::Ok), &QAbstractButton::clicked,
            this, [this, cryptoConfWidget, gpgolConfWidget, gpgolDbgWidget] () {
        if (cryptoConfWidget) {
            cryptoConfWidget->save();
        }
        gpgolConfWidget->save();
        gpgolDbgWidget->save();
        close();
    });
    connect(buttonBox->button(QDialogButtonBox::RestoreDefaults), &QAbstractButton::clicked,
            this, [this, cryptoConfWidget, gpgolConfWidget, gpgolDbgWidget] () {

        if (currentPage()->widget() == cryptoConfWidget) {
            cryptoConfWidget->defaults();
        } else if (currentPage()->widget() == gpgolConfWidget){
            gpgolConfWidget->defaults();
        } else if (currentPage()->widget() == gpgolDbgWidget){
            gpgolDbgWidget->defaults();
        }
    });
    connect(buttonBox->button(QDialogButtonBox::Cancel), &QAbstractButton::clicked,
            this, [this] () {
        close();
    });

    KPageWidgetItem *page = new KPageWidgetItem(gpgolConfWidget, _("GpgOL"));
    page->setHeader(QStringLiteral("%1 - %2%3").arg(_("Configure GpgOL")).arg(
                      _("Version ")).arg(mVersion));
    page->setIcon(QIcon(":/gpgol-icon.svg"));
    addPage(page);

    if (cryptoConfWidget) {
        page = new KPageWidgetItem(cryptoConfWidget, QStringLiteral("%1\n%2").arg(_("GnuPG System")).arg(_("(Technical)")));
        page->setHeader(_("Configuration of GnuPG System options"));
        page->setIcon(QIcon(":/document-encrypt.svg"));
        addPage(page);
    }

    page = new KPageWidgetItem(gpgolDbgWidget, _("Debug"));
    page->setHeader(_("Configuration of debug options"));
    page->setIcon(QIcon(":/tools-report-bug.svg"));
    addPage(page);
}
