#ifndef GPGOLCONFIGPAGE_H
#define GPGOLCONFIGPAGE_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
#include <QWidget>
#include <QMap>
#include <QString>

class QGroupBox;
class QCheckBox;
class QLabel;
class ExplainingChkBox;

namespace Kleo
{
class KeySelectionCombo;
} // namespace Kleo

class GpgOLConfigPage: public QWidget
{
    Q_OBJECT

public:
    explicit GpgOLConfigPage(QWidget *parent = nullptr);

    void save() const;
    void load();
    void defaults();

protected:
    void setupGUI();
    void updateGUI(const QMap<QString, std::pair <bool, bool> > &values);

private:
    QGroupBox *mSMIMEGrp,
              *mAutomationGrp;
    QLabel    *mSearchSMIMEWarning;
    QCheckBox *mPreferSMIMEChk,
              *mAutoSecureChk,
              *mAlwaysEncChk,
              *mAlwaysSigChk,
              *mInlinePGPChk,
              *mAutoTrustChk,
              *mAutoResolveChk,
              *mReplyCryptChk,
              *mSearchSMIMEChk,
              *mAutoEncryptUntrustedChk,
              *mAutoImportChk,
              *mDraftEncChk,
              *mAlwaysShowApprovalChk;
    Kleo::KeySelectionCombo *mDraftKey;
};

#endif
