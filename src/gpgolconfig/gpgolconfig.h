#ifndef GPGOLCONFIG_H
#define GPGOLCONFIG_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <KPageDialog>
#include <QString>

class QCommandLineParser;

class GpgOLConfig: public KPageDialog
{
    Q_OBJECT

public:
    GpgOLConfig(const QCommandLineParser &parser);

protected:
    /** @brief UI setup */
    void setupGUI();

private:
    bool mCryptoConfigChanged;
    QString mVersion;
};
#endif // GPGOLCONFIG_H
