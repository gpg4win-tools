/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#ifndef CRYPTOCONFIGPAGE_H
#define CRYPTOCONFIGPAGE_H

#include <QWidget>

namespace Kleo
{
  class CryptoConfigModule;
} // namespace Kleo

namespace QGpgME
{
  class CryptoConfig;
}

class CryptoConfigPage: public QWidget
{
    Q_OBJECT

public:
    explicit CryptoConfigPage(QWidget *parent = nullptr);

    void save();
    void load();
    void defaults();

private Q_SLOTS:
    void delayLoadFinished(QGpgME::CryptoConfig *config);

private:
    Kleo::CryptoConfigModule *mConfigWidget;
    bool mCryptoConfigChanged;
};
#endif
