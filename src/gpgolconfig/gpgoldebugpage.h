#ifndef GPGOLDEBUGPAGE_H
#define GPGOLDEBUGPAGE_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */
#include <QWidget>
#include <QMap>
#include <QString>

class QGroupBox;
class QCheckBox;
class QLabel;
class QLineEdit;
class QComboBox;
class QPushButton;

class GpgOLDebugPage: public QWidget
{
    Q_OBJECT

public:
    explicit GpgOLDebugPage(QWidget *parent = nullptr);

    void save() const;
    void load();
    void defaults();

protected:
    void setupGUI();
    void updateGUI(const QMap<QString, bool> &values);
    void enableDisableDbgWidgets();

private:
    QGroupBox *mDbgGrp;
    QCheckBox *mSyncEncChk,
              *mSyncDecChk,
              *mNoSaveChk,
              *mDbgDataChk;
    QLineEdit *mDbgLogFileName;
    QLabel    *mDbgLogFileLabel,
              *mDbgComboLabel,
              *mDbgVerboseWarningLabel;
    QComboBox *mDbgCombo;
    QPushButton *mDbgLogFileBtn;
};

#endif
