/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include "cryptoconfigpage.h"

#include <QThread>
#include <QVBoxLayout>
#include <QProgressBar>
#include <QLabel>
#include <QDebug>

#include <QGpgME/Protocol>
#include <QGpgME/CryptoConfig>

#include <Libkleo/CryptoConfigModule>

void clearLayout(QLayout* layout, bool deleteWidgets = true)
{
    while (QLayoutItem* item = layout->takeAt(0))
    {
        if (deleteWidgets)
        {
            if (QWidget* widget = item->widget())
                widget->deleteLater();
        }
        if (QLayout* childLayout = item->layout())
            clearLayout(childLayout, deleteWidgets);
        delete item;
    }
}

class DelayLoader: public QThread
{
    Q_OBJECT
    void run() override {
        QGpgME::CryptoConfig *config = QGpgME::cryptoConfig();

        /* Grab an entry to force the load */
        auto entry = config->entry(QStringLiteral("gpg"),
                                   QStringLiteral("Keyserver"),
                                   QStringLiteral("keyserver"));
        Q_UNUSED(entry);

        emit resultReady(config);
        deleteLater();
    }
signals:
    void resultReady(QGpgME::CryptoConfig *config);
};

void CryptoConfigPage::delayLoadFinished(QGpgME::CryptoConfig *config)
{
    mConfigWidget = new Kleo::CryptoConfigModule(config);
    auto lay = layout();
    clearLayout(lay, true);
    delete lay;
    auto newLay = new QVBoxLayout(this);
    newLay->addWidget(mConfigWidget);
    connect(mConfigWidget, &Kleo::CryptoConfigModule::changed,
          this, [this] () {
          mCryptoConfigChanged = true;
    });
}

CryptoConfigPage::CryptoConfigPage(QWidget *parent):
    QWidget(parent),
    mConfigWidget(nullptr),
    mCryptoConfigChanged(false)
{
    auto loader = new DelayLoader;
    auto vLay = new QVBoxLayout(this);
    auto bar = new QProgressBar;
    auto label = new QLabel;
    label->setText(QStringLiteral("<h3>Loading module...</h3>"));
    bar->setRange(0, 0);
    vLay->addStretch(1);

    auto subLay1 = new QVBoxLayout;
    auto subLay3 = new QHBoxLayout;
    subLay3->addStretch(0.5);
    subLay3->addWidget(label);
    subLay3->addStretch(1);
    subLay1->addLayout(subLay3);
    subLay1->addWidget(bar);

    auto subLay2 = new QHBoxLayout;
    subLay2->addStretch(0.1);
    subLay2->addLayout(subLay1);
    subLay2->addStretch(0.1);

    vLay->addLayout(subLay2);

    vLay->addStretch(1);

    connect(loader, SIGNAL(resultReady(QGpgME::CryptoConfig *)),
            this, SLOT(delayLoadFinished(QGpgME::CryptoConfig *)));

    /*
    connect(loader, &DelayLoader::resultReady, [this] (QGpgME::CryptoConfig *config) {
        qDebug() << "Creating config widget";
        mConfigWidget = new Kleo::CryptoConfigModule(config,
                                                      Kleo::CryptoConfigModule::TabbedLayout);
        delete layout();
        auto newLay = new QVBoxLayout(this);
        newLay->addWidget(mConfigWidget);
        connect(mConfigWidget, &Kleo::CryptoConfigModule::changed,
                this, [this] () {
            mCryptoConfigChanged = true;
        });
    });
    */
    loader->start();
}

void CryptoConfigPage::save()
{
    if (mConfigWidget && mCryptoConfigChanged) {
        mConfigWidget->save();
    }
}

void CryptoConfigPage::defaults()
{
    if (mConfigWidget) {
        mConfigWidget->defaults();
    }
}

#include "cryptoconfigpage.moc"
