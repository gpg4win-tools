#ifndef STDINREADER_H
#define STDINREADER_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QThread>
#include <QByteArray>

class StdinReader: public QThread
{
    Q_OBJECT;

Q_SIGNALS:
    void stdinRead(const QByteArray &data);

protected:
    virtual void run() override;

private:
    QByteArray mData;
};

#endif // STDINREADER_H
