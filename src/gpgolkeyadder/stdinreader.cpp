/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include "stdinreader.h"
#include <iostream>

#include <QApplication>
static void
rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

void StdinReader::run()
{
    std::string line;
    while (true) {
        std::getline(std::cin, line);
        // Normalize line endings
        rtrim(line);
        line += "\n";

        mData += QByteArray::fromStdString(line);
        if (std::cin.eof()) {
            Q_EMIT stdinRead(mData);
            deleteLater();
            return;
        }
    }
}
