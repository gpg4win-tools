#ifndef GPGOLKEYADDER_H
#define GPGOLKEYADDER_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QDialog>
#include <QString>

#include <vector>
#include <gpgme++/key.h>

class QCommandLineParser;
class QTextEdit;
class QCheckBox;


class GpgOLKeyAdder: public QDialog
{
    Q_OBJECT

public:
    GpgOLKeyAdder(const QCommandLineParser &parser);

protected:
    /** @brief UI setup */
    void setupGUI();

private:
    void checkAccept();
    void checkAcceptBottom(const std::vector<GpgME::Key> &pgpKeys,
                           const std::vector<GpgME::Key> &cmsKeys,
                           const std::string &error);
    void save();
    void handleInput(const QByteArray &data);

    QTextEdit *mEdit;
    QTextEdit *mCMSEdit;
    QString mName;
    QCheckBox *mAlwaysSec;
    bool mShowCMS;
};
#endif // GPGOLKEYADDER_H
