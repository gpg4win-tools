#ifndef GPGOLKEYADDER_OPTIONS
#define GPGOLKEYADDER_OPTIONS
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QCommandLineParser>
#include <QList>

/** @file Commandline options*/
static void options(QCommandLineParser &parser)
{
    QList<QCommandLineOption> options;

    options
        << QCommandLineOption(QStringList() << QStringLiteral("debug"),
                QStringLiteral("Print debug output."))
        << QCommandLineOption(QStringList() << QStringLiteral("cms"),
                QStringLiteral("Add S/MIME settings"))
        << QCommandLineOption(QStringLiteral("hwnd"),
                QStringLiteral("Parent Window"),
                QStringLiteral("windows window handle"))
        << QCommandLineOption(QStringList() << QStringLiteral("sign"),
                QStringLiteral("Always sign"))
        << QCommandLineOption(QStringList() << QStringLiteral("encrypt"),
                QStringLiteral("Always encrypt"))
        << QCommandLineOption(QStringLiteral("username"),
                QStringLiteral("Name"),
                QStringLiteral("username"))
        << QCommandLineOption(QStringLiteral("lang"),
                QStringLiteral("Language"),
                QStringLiteral("Language to be used e.g. de_DE"));

    for (const auto &opt: options) {
        parser.addOption(opt);
    }
    parser.addVersionOption();
    parser.addHelpOption();
}
#endif
