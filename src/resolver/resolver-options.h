#ifndef RESOLVER_OPTIONS
#define RESOLVER_OPTIONS
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */

#include <QCommandLineParser>
#include <QList>

/** @file Commandline options*/
static void options(QCommandLineParser &parser)
{
    QList<QCommandLineOption> options;

    options
        << QCommandLineOption(QStringList() << QStringLiteral("debug"),
                QStringLiteral("Print debug output."))
        << QCommandLineOption(QStringList() << QStringLiteral("allowMixed"),
                QStringLiteral("Allow a combination of S/MIME and OpenPGP"))
        << QCommandLineOption(QStringLiteral("protocol"),
                QStringLiteral("Specify a forced protocol"),
                QStringLiteral("pgp or cms"))
        << QCommandLineOption(QStringLiteral("preferred-protocol"),
                QStringLiteral("Specify a preferred protocol"),
                QStringLiteral("pgp or cms"))
        << QCommandLineOption(QStringLiteral("sender"),
                QStringLiteral("The sender"),
                QStringLiteral("sender mailbox"))
        << QCommandLineOption(QStringLiteral("sign"),
                QStringLiteral("Should be signed"))
        << QCommandLineOption(QStringLiteral("encrypt"),
                QStringLiteral("Should be encrypted"))
        << QCommandLineOption(QStringLiteral("hwnd"),
                QStringLiteral("Parent Window"),
                QStringLiteral("windows window handle"))
        << QCommandLineOption(QStringLiteral("overlayText"),
                QStringLiteral("Overlay Text"),
                QStringLiteral("text to overlay over hwnd"))
        << QCommandLineOption(QStringLiteral("lang"),
                QStringLiteral("Language"),
                QStringLiteral("Language to be used e.g. de_DE"))
        << QCommandLineOption(QStringList() << QStringLiteral("override")
                                            << QStringLiteral("o"),
                                        QStringLiteral("Override where format can be:\n"
                                                       "InlineOpenPGP\n"
                                                       "OpenPGPMIME\n"
                                                       "SMIME\n"
                                                       "SMIMEOpaque\n"
                                                       "AnyOpenPGP\n"
                                                       "AnySMIME\n"
                                                       "Auto"),
                                        QStringLiteral("mailbox:fpr,fpr,..:format"))
        << QCommandLineOption(QStringLiteral("alwaysShow"),
                              QStringLiteral("Should always be shown"));

    for (const auto &opt: options) {
        parser.addOption(opt);
    }
    parser.addVersionOption();
    parser.addHelpOption();

    parser.addPositionalArgument(QStringLiteral("recipients"),
                                 QStringLiteral("Recipient Mailboxes"),
                                 QStringLiteral("[recipients]"));
}
#endif // RESOLVER_OPTIONS
