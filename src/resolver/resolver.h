#ifndef RESOLVER_H
#define RESOLVER_H
/* Copyright (C) 2018 by Intevation GmbH <info@intevation.de>
 *
 * This file is free software under the GNU GPL (v>=2)
 * and comes with ABSOLUTELY NO WARRANTY!
 * See LICENSE.txt for details.
 */


#include <QApplication>
#include <QStringList>
#include <memory>

class QCommandLineParser;
class QString;

/** @brief Resolver Application entry point.
 *
 * The Resolver application exists to resolve Mailboxes
 * to key fingerprints. A typical task for a MUA.
 *
 * The main user of this app is GpgOL so it is tailored
 * to GpgOL's needs.
 *
 * This is the global qApp singleton. */
class Resolver: public QApplication
{
    Q_OBJECT
public:
    /** Create a new Application obejct. You have to
     * make sure to call init afterwards to get a valid object.
     * This is to delay initialisation after the UniqueService
     * call is done and our init / call might be forwarded to
     * another instance. */
    explicit Resolver(int &argc, char *argv[]);

    static Resolver *instance()
    {
        return static_cast<Resolver*> (qApp);
    }

    /** Starts a new instance or a command from the command line.
     *
     * Handles the parser options and starts the according commands.
     * The parser should have been initialized with options and
     * already processed.
     *
     * @param parser: The command line parser to use.
     *
     * @returns an empty QString on success. A error message otherwise.
     * */
    QString newInstance(const QCommandLineParser &parser);


private:
    class Private;
    std::shared_ptr<Private> d;
};
#endif
